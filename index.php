<?php

// Main abstract class.
abstract class RuleAbstract {
  // Needed build content method.
  abstract public function generateContent();
  // Needed set content method.
  abstract public function setContent($content);
  // Needed get content method.
  abstract public function getContent();
}

// Create main class.
class Rule extends RuleAbstract {
  private $content;

  public function generateContent() {
    // TODO: Implement generateContent() method.
  }

  public function setContent($content) {
    // TODO: Implement getContent() method.
    $this->content = $content;
  }

  public function getContent() {
    // TODO: Implement getContent() method.
    return $this->content;
  }
}

// Create Poem class.
class Poem extends Rule {
  private $lines;

  public function __construct($lines) {
    $this->lines = $lines;
  }

  public function generateContent() {
    $object_lines = [];
    for ($i = 0; $i < $this->lines; $i++) {
      $new_line = new Line();
      $new_line->generateContent();
      $object_lines[] = $new_line->getContent();
    }
    $this->setContent(implode(' ', $object_lines));
  }
}

// Create Line class.
class Line extends Rule {
  public function generateContent() {
    // Add random rule.
    $composition = ['Noun', 'Preposition', 'Pronoun',];
    $class = new $composition[array_rand($composition)]();
    $class->generateContent();
    $content = $class->getContent();

    // Generate keyword $linebrak;
    $keyword = new KeyWord(['$linebreak']);
    $keyword->generateContent();
    $content .= ' ' . $keyword->getContent();
    $this->setContent($content);
  }
}

// Create Adjective class.
class Adjective extends Rule {
  public function generateContent() {
    // Generate word.
    $word = new Word(explode('|', 'black|white|dark|light|bright|murky|muddy|clear'));
    $word->generateContent();
    $content = $word->getContent();

    // Add random rule.
    $composition = ['Noun', 'Adjective', 'Keyword',];
    $class_name = $composition[array_rand($composition)];
    if ($class_name != 'Keyword') {
      // Only continue if $class_name is different of keyword ($end as only option).
      $class = new $class_name();
      $class->generateContent();
      $content .= ' ' . $class->getContent();
    }
    $this->setContent($content);
  }
}

// Create Noun class.
class Noun extends Rule {
  public function generateContent() {
    // Generate word.
    $word = new Word(explode('|', 'heart|sun|moon|thunder|fire|time|wind|sea|river|flavor|wave|willow|rain|tree|flower|fi
eld|meadow|pasture|harvest|water|father|mother|brother|sister'));
    $word->generateContent();
    $content = $word->getContent();

    // Add random rule.
    $composition = ['Verb', 'Preposition', 'Keyword',];
    $class_name = $composition[array_rand($composition)];
    if ($class_name != 'Keyword') {
      // Only continue if $class_name is different of keyword ($end as only option).
      $class = new $class_name();
      $class->generateContent();
      $content .= ' ' . $class->getContent();
    }
    $this->setContent($content);
  }
}

// Create Pronoun class.
class Pronoun extends Rule {
  public function generateContent() {
    // Generate word.
    $word = new Word(explode('|', 'my|your|his|her'));
    $word->generateContent();
    $content = $word->getContent();

    // Add random rule.
    $composition = ['Noun', 'Adjective',];
    $class = new $composition[array_rand($composition)]();
    $class->generateContent();
    $content .= ' ' . $class->getContent();
    $this->setContent($content);
  }
}

// Create Verb class.
class Verb extends Rule {
  public function generateContent() {
    // Generate word.
    $word = new Word(explode('|', 'runs|walks|stands|climbs|crawls|flows|flies|transcends|ascends|descends|sinks'));
    $word->generateContent();
    $content = $word->getContent();

    // Add random rule.
    $composition = ['Preposition', 'Pronoun', 'Keyword',];
    $class_name = $composition[array_rand($composition)];
    if ($class_name != 'Keyword') {
      // Only continue if $class_name is different of keyword ($end as only option).
      $class = new $class_name();
      $class->generateContent();
      $content .= ' ' . $class->getContent();
    }
    $this->setContent($content);
  }
}

// Create Preposition class.
class Preposition extends Rule {
  public function generateContent() {
    // Generate word.
    $word = new Word(explode('|', 'above|across|against|along|among|around|before|behind|beneath|beside|between|beyond|du
ring|inside|onto|outside|under|underneath|upon|with|without|through'));
    $word->generateContent();
    $content = $word->getContent();

    // Add random rule.
    $composition = ['Noun', 'Pronoun', 'Adjective',];
    $class = new $composition[array_rand($composition)]();
    $class->generateContent();
    $content .= ' ' . $class->getContent();
    $this->setContent($content);
  }
}

// Create KeyWord class.
class KeyWord extends Rule {
  private $options;

  // Set options.
  public function __construct($options = []) {
    $this->options = $options;
  }

  // Random generation from options.
  public function generateContent() {
    $this->setContent($this->options[array_rand($this->options)]);
  }
}

// Create Word class.
class Word extends KeyWord {
  // Same as Keyword.
}

// Generate Poem with 5 lines.
$poem = new Poem(5);
$poem->generateContent();
// Replacing $linebreak.
print str_replace('$linebreak', '<br /><br />', $poem->getContent());
print '<br /><br />';
print '<img alt="poem" title="poem" src="https://picsum.photos/200/300/?random" />';
